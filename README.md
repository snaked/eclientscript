Original extension: http://www.yiiframework.com/extension/eclientscript/

Using this extension is as simple as adding the following code to the application configuration under the components array:

	:::php
	'clientScript' => array(
		'class' => 'ext.minify.EClientScript',
		'combineScriptFiles' => true, // By default this is set to false, set this to true if you'd like to combine the script files
		'combineCssFiles' => true, // By default this is set to false, set this to true if you'd like to combine the css files
		'optimizeCssFiles' => false,
		'optimizeScriptFiles' => false,

		// CSS files for ignore
		'cssForIgnore' => array('bootstrap.min.css', 'jquery-ui-1.7.1.custom.css', 'jquery-ui.multiselect.css'),

		// JS files for ignore
		'scriptsForIgnore' => array('jquery.js', 'jquery.min.js', 'jquery.ui.js', 'jquery-ui.min.js', 'bootstrap.min.js'),
	),

Then you'd use the regular 'registerScriptFile' & 'registerCssFile' methods as normal and the files will be combined automatically.

## Что было добавлено/исправлено
- Добавил параметры, указывающие какие js/css файлы игнорировать. Актуально для больших файлов (например, jQuery).
- Обновил версии CssMin (http://code.google.com/p/cssmin/) и JSMinPlus (http://crisp.tweakblogs.net/blog/cat/716)
- Исправлена data:image когда генерировались запросы к серверу, теперь не дописывает пути

## 09.10.13 Добавлено/исправлено
- добавлена возможность задавать media для css-файлов при настройке пакетов в конфиге (например, array('css/print-only.css', array('media' => 'print')));
- добавлена возможность задавать условные комментарии для IE (через media, например registerCssFile('/css/ie.css', 'lte IE 6'));
- исправлена ошибка с созданием пустых файлов при разных media